import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DATASALUD } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class DatosService {

  constructor(private http: HttpClient) { }

  getData(){
    return this.http.get<DATASALUD[]>('https://www.datos.gov.co/resource/emmz-uf6m.json')
  }
}
