import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatosService } from '../datos.service';
import { DATASALUD } from '../interfaces';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css']
})
export class DatosComponent implements OnInit {

  displayedColumns: string[] = [
    'index',
    'ips',
    'fechaAsignacionRegistro',
    'sexo',
    'edad',
    'horaSalida',
    'impresionesDiagnosticas',
    'objetoRemision'
  ];

  constructor(private datosService: DatosService,
              private _liveAnnouncer: LiveAnnouncer) { }
  dataSource = null;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  ngAfterViewInit() {
  }
  
  ngOnInit(): void {
    this.datosService.getData()
    .subscribe(res=>{
      const resNew = res.map((item, index)=>{
        return {index:(index+1),...item}
      })
      this.dataSource = new MatTableDataSource<DATASALUD>(resNew);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  announceSortChange(sortState: Sort){
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}
